<?php

namespace Elogic\Vendor\Api;

use Elogic\Vendor\Api\Data\VendorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;

/**
 * @api
 */
interface VendorRepositoryInterface
{
    /**
     * Save page.
     *
     * @param VendorInterface $vendor
     * @return VendorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(VendorInterface $vendor);

    /**
     * Retrieve
     *
     * @param int $vendorId
     * @return VendorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($vendorId);

    /**
     * Delete
     *
     * @param VendorInterface $vendor
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(VendorInterface $vendor);

    /**
     * Delete by ID.
     *
     * @param int $vendorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($vendorId);

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults;
}
