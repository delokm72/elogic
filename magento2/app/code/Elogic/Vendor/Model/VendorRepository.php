<?php

namespace Elogic\Vendor\Model;

use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Exception\NoSuchEntityException;

use Elogic\Vendor\Api\VendorRepositoryInterface;
use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Model\VendorFactory;

use Elogic\Vendor\Model\ResourceModel\Vendor\Collection;
use Elogic\Vendor\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;
use Elogic\Vendor\Model\ResourceModel\Vendor;

class VendorRepository implements VendorRepositoryInterface
{
    /**
     * @var VendorFactory
     */
    private $vendorFactory;

    /**
     * @var VendorCollectionFactory
     */
    private $vendorCollectionFactory;

    /**
     * @var Vendor
     */
    private $resource;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @type SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @type CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param VendorFactory $vendorFactory
     * @param VendorCollectionFactory $vendorCollectionFactory
     * @param Vendor $resource
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        VendorFactory $vendorFactory,
        VendorCollectionFactory $vendorCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Vendor $resource,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->vendorFactory = $vendorFactory;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->resource = $resource;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param vendorInterface $vendor
     * @return vendorInterface
     * @throws CouldNotSaveException
     */
    public function save(vendorInterface $vendor)
    {
        try {
            /** @var vendorInterface|\Magento\Framework\Model\AbstractModel $vendor */
            $this->resource->save($vendor);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the vendor: %1',
                $exception->getMessage()
            ));
        }
        return $vendor;
    }

    /**
     * Get record
     *
     * @param $vendorId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($vendorId)
    {
        if (!isset($this->instances[$vendorId])) {
            $vendor = $this->vendorFactory->create();
            $this->resource->load($vendor, $vendorId);
            if (!$vendor->getId()) {
                throw new NoSuchEntityException(__('Requested record doesn\'t exist'));
            }
            $this->instances[$vendorId] = $vendor;
        }
        return $this->instances[$vendorId];
    }

    /**
     * @param VendorInterface $vendor
     * @return bool
     * @throws CouldNotSaveException
     * @throws StateException
     */
    public function delete(VendorInterface $vendor)
    {
        /** @var \Elogic\Vendor\Api\Data\VendorInterface|\Magento\Framework\Model\AbstractModel $vendor */
        $id = $vendor->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($vendor);
        } catch (ValidatorException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new StateException(
                __('Unable to remove record %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * @param $vendorId
     * @return bool
     */
    public function deleteById($vendorId)
    {
        $vendor = $this->getById($vendorId);
        return $this->delete($vendor);
    }

    /**
     * {@inheritDoc}
     */
    public function getList(SearchCriteriaInterface $criteria): SearchResults
    {
        /** @var Collection $collection */
        $collection = $this->vendorCollectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);
        /** @var SearchResults $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function getNameById($id)        //ONLY for getNameByIdArray()!
    {
        $items = $this->getCollectionForId($id);
        /** @var VendorInterface $vendor */
        $vendor = array_shift($items);
        return $vendorName = $vendor->getName();
    }

    public function getCollectionForId($id)  //ONLY for getNameByIdArray()!
    {
        /** @var SearchCriteriaInterface $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(VendorInterface::ID, $id)
            ->create();
        $vendorCollection = $this->getList($searchCriteria)->getItems();
        return $vendorCollection;
    }
}
