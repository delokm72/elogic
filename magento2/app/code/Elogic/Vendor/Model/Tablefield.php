<?php

namespace Elogic\Vendor\Model;

use Elogic\Vendor\Api\VendorRepositoryInterface;
use Elogic\Vendor\Model\Vendor;
use Elogic\Vendor\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;

use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\View\Element\Template\Context;

class Tablefield extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $_options;

    /**
     * @var VendorRepositoryInterface
     */
    private $vendorRepository;
    /**
     * @var VendorCollectionFactory
     */
    private $vendorCollectionFactory;

    /**
     * @var \Elogic\Vendor\Model\ResourceModel\Vendor\Collection|null
     */
    private $vendorCollection;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param Context $context
     * @param VendorCollectionFactory $vendorCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param VendorRepositoryInterface $vendorRepository
     * @param SortOrderBuilder $sortOrderBuilder
     *
     */

    public function __construct(
        // \Magento\Framework\App\ResourceConnection $resource,
        // Context $context,
        VendorCollectionFactory $vendorCollectionFactory,
        VendorRepositoryInterface $vendorRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->vendorRepository = $vendorRepository;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getAllOptions()
    {
        /** @var SearchCriteria|SearchCriteriaInterface $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchResults = $this->vendorRepository->getList($searchCriteria);
        $options = [];
        $vendorCollection = $searchResults->getItems();

        foreach ($vendorCollection as $vendor) {
            /** @var VendorModel $vendor */
            $options[] = ['label' => $vendor->getName(), 'value' => $vendor->getId()];
        }
        return $options;
    }
}
