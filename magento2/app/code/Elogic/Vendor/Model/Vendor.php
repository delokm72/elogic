<?php

namespace Elogic\Vendor\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Exception\LocalizedException;
use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Model\ResourceModel\Vendor as VendorResourceModel;

class Vendor extends AbstractModel implements VendorInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'vendor';

    /**
     * @var UploaderPool
     */
    protected $uploaderPool;

    /**
     * Sliders constructor.
     * @param Context $context
     * @param Registry $registry
     * @param UploaderPool $uploaderPool
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        UploaderPool $uploaderPool,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->uploaderPool    = $uploaderPool;
       }

    /**
     * Initialise resource model
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(VendorResourceModel::class);
    }

    /**
     * Get cache identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    public function getName()
    {
        return $this->getData(self::NAME);
    }

    public function getId()
    {
        return $this->getData(self::ID);
    }

    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Set
     *
     * @param $image
     * @return $this
     */
    public function setImage($image)
    {
        return $this->setData(VendorInterface::IMAGE, $image);
    }

    /**
     * Get URL
     *
     * @return bool|string
     * @throws LocalizedException
     */
    public function getImageUrl()
    {
        $url = false;
        $image = $this->getImage();
        if ($image) {
            if (is_string($image)) {
                $uploader = $this->uploaderPool->getUploader('image');
                $url = $uploader->getBaseUrl().$uploader->getBasePath().$image;
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
}
