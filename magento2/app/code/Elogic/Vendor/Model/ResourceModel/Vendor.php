<?php

namespace Elogic\Vendor\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Vendor extends AbstractDb
{
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init('vendor', 'id');
    }
}
