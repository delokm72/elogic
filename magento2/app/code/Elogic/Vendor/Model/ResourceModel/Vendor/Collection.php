<?php

namespace Elogic\Vendor\Model\ResourceModel\Vendor;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        $this->_init(
            'Elogic\Vendor\Model\Vendor',
            'Elogic\Vendor\Model\ResourceModel\Vendor'
        );
    }
}
