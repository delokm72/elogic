<?php

namespace Elogic\Vendor\Block;

use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Elogic\Vendor\Api\Data\VendorInterface;
use Elogic\Vendor\Api\VendorRepositoryInterface;
use Elogic\Vendor\Model\ResourceModel\Vendor\CollectionFactory as VendorCollectionFactory;
use Elogic\Vendor\Model\ResourceModel\Vendor\Collection as VendorCollection;
use Elogic\Vendor\Model\Vendor;

class VendorImage extends Template implements ArgumentInterface
{
    /**
     * @var VendorCollection|null
     */
    private $vendorCollection;

    /**
     * @var VendorCollectionFactory
     */
    private $vendorCollectionFactory;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var vendorRepositoryInterface
     */
    private $vendorRepository;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @param Context $context
     *
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param VendorRepositoryInterface $vendorRepository
     * @param SortOrderBuilder $sortOrderBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        VendorCollectionFactory $vendorCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        VendorRepositoryInterface $vendorRepository,
        \Magento\Framework\Registry $registry,
        SortOrderBuilder $sortOrderBuilder,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_coreRegistry = $registry;
        $this->vendorCollectionFactory = $vendorCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->vendorRepository = $vendorRepository;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    protected function _prepareLayout()
    {
        //
    }

    public function getNameByIdArrayUpd($idString)   // UPDATED getNameByIdArray()
    {
        $resultArray = [];
        $temp = $this->getVendorCollection($idString);
        foreach ($temp as $vendor) {
            /** @var Vendor $vendor */
            $resultArray[] = $vendor->getName();
        }
        return $resultArray;
    }

    public function getNameByIdArray($idString)
    {
        if (!is_string($idString)) return null;
        $resultArray = [];
        $pieces = explode(",", $idString);
        foreach ($pieces as $value) {
            $resultArray[] = $this->vendorRepository->getNameById($value);
        }
        return $resultArray;
    }

    public function getVendorCollection($idString)
    {
        try {
            $sortOrder = $this->sortOrderBuilder
                ->create();
            /** @var SearchCriteria|SearchCriteriaInterface $searchCriteria */
            $searchCriteria = $this->searchCriteriaBuilder->addFilter(Vendor::ID, $idString, 'in')
                ->addSortOrder($sortOrder)->create();
            /** @var SearchResults $searchResults */
            $searchResults = $this->vendorRepository->getList($searchCriteria);
            if ($searchResults->getTotalCount() > 0) {
                $this->vendorCollection = $searchResults->getItems();
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
        return $this->vendorCollection;
    }

    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }
}
