<?php

namespace Elogic\Vendor\Block\Adminhtml\Image\Edit\Buttons;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Elogic\Vendor\Api\VendorRepositoryInterface;

class Generic
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var VendorRepositoryInterface
     */
    protected $vednorRepository;

    /**
     * @param Context $context
     * @param VendorRepositoryInterface $vendorRepository
     */
    public function __construct(
        Context $context,
        VendorRepositoryInterface $vendorRepository
    ) {
        $this->context = $context;
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * Return Image ID
     *
     * @return int|null
     */
    public function getImageId()
    {
        try {
            return $this->vendorRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
