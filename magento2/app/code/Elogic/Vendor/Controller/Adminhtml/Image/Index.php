<?php

namespace Elogic\Vendor\Controller\Adminhtml\Image;

use Elogic\Vendor\Controller\Adminhtml\Image;

class Index extends Image
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Vendor::image');
        $resultPage->getConfig()->getTitle()->prepend(__('Vendor'));
        $resultPage->addBreadcrumb(__('Vendor'), __('Vendor'));
        return $resultPage;
    }
}
