<?php

namespace Elogic\Vendor\Controller\Adminhtml\Image;

use Elogic\Vendor\Controller\Adminhtml\Image;

class Edit extends Image
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $imageId = $this->getRequest()->getParam('id');
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Vendor::image')
            ->addBreadcrumb(__('Vendors'), __('Vendors'))
            ->addBreadcrumb(__('Manage Vendors'), __('Manage Vendors'));

        if ($imageId === null) {
            $resultPage->addBreadcrumb(__('New Vendor'), __('New Vendor'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Vendor'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Vendor'), __('Edit Vendor'));
            $resultPage->getConfig()->getTitle()->prepend(__('Edit Vendor'));
        }
        return $resultPage;
    }
}
