<?php

namespace Elogic\Vendor\Controller\Adminhtml\Image;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Elogic\Vendor\Controller\Adminhtml\Image;

class Delete extends Image
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $vendorId = $this->getRequest()->getParam('id');
        if ($vendorId) {
            try {
                $this->vendorRepository->deleteById($vendorId);
                $this->messageManager->addSuccessMessage(__('The record has been deleted.'));
                $resultRedirect->setPath('vendor/image/index');
                return $resultRedirect;
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The image no longer exists.'));
                return $resultRedirect->setPath('vendor/image/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('vendor/image/index', ['id' => $vendorId]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the image'));
                return $resultRedirect->setPath('vendor/image/edit', ['id' => $vendorId]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find the image to delete.'));
        $resultRedirect->setPath('vendor/image/index');
        return $resultRedirect;
    }
}
