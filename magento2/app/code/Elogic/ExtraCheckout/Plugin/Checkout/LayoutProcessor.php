<?php

namespace Elogic\ExtraCheckout\Plugin\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor as L_Processor;
use \Psr\Log\LoggerInterface;

class LayoutProcessor
{
    protected $logger;
    public function __construct(
        LoggerInterface $logger
    )
    {
        $this->logger = $logger;
    }

    public function afterProcess(
        L_Processor $subject,
        array $jsLayout
    )
    {
        $jsLayout['components']['checkout']['children']['steps']['children']
        ['facebook-step']
        ['children']
        ['facebook_profile'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'options' => []

            ],
            'dataScope' => 'custom_attributes.facebook_profile',
            'label' => 'facebook account',
            'provider' => 'checkoutProvider',
            'visible' => 'true',
            'validation' => []


        ];
        return $jsLayout;
    }
}
