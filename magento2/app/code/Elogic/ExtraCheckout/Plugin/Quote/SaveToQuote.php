<?php

namespace Elogic\ExtraCheckout\Plugin\Quote;

use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Framework\Message\ManagerInterface;

class SaveToQuote
{
    protected $quoteRepository;
    protected $_checkoutSession;

    function __construct(
        QuoteRepository $quoteRepository,
        ManagerInterface $messageManager
    )
    {
        $this->messageManager = $messageManager;
        $this->quoteRepository = $quoteRepository;
    }

    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    )
    {
        if (!$extAttributes = $addressInformation->getExtensionAttributes()) {
            return;
        }
        $this->messageManager->addNoticeMessage('Lets start! .....');
        $quote = $this->quoteRepository->getActive($cartId);
        //   $data = json_decode(file_get_contents('php://input'), true);
        //    $subdistrict = $data['facebook-proile']['custom_attributes']['facebook_proile'];
        $quote->setFacebookProfile($extAttributes->getFacebookProfile());
        // $quote->setFacebookProfile('ababagalamaga');

        if ($quote) {
            $this->messageManager->addNoticeMessage('Settings is applied by FB QUOTA');
        }
        $quote->save();
    }
}
