var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Elogic_ExtraCheckout/js/view/shipping-mixin': false
            }
        }
    },
    "map": {
        "*": {
            "Magento_Checkout/js/model/shipping-save-processor/default" :
                "Elogic_ExtraCheckout/js/shipping-save-processor"
        }
    }
};
