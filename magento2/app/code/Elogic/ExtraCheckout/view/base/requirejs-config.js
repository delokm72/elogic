var config = {
    'config': {
        'mixins': {
            'Magento_Checkout/js/view/shipping': {
                'Elogic_ExtraCheckout/js/view/shipping-payment-mixin': true
            },
            'Magento_Checkout/js/view/payment': {
                'Elogic_ExtraCheckout/js/view/shipping-payment-mixin': true
            }
        }
    }
}
