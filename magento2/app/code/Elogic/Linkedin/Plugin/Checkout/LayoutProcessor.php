<?php

namespace Elogic\Linkedin\Plugin\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor as L_Processor;
use \Psr\Log\LoggerInterface;

class LayoutProcessor
{
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->_checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    public function afterProcess(
        L_Processor $subject,
        array $jsLayout
    )
    {
        $customAttributeCode = 'linkedin_profile';   //point!

        $customField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'tooltip' => [
                    'description' => 'Linkedin URL of customer',
                ],
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => 'Customer linkedin URL',
            'provider' => 'checkoutProvider',
            'sortOrder' => 15,
            'validation' => [
                "validate-url" => true,
                "max_text_length" => 250
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];
        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children'][$customAttributeCode] = $customField;
        return $jsLayout;
    }
}
