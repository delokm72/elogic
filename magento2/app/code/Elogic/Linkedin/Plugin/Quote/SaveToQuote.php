<?php

namespace Elogic\Linkedin\Plugin\Quote;

use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Checkout\Api\Data\ShippingInformationInterface;

class SaveToQuote    //save item to quota table
{
    protected $quoteRepository;

   function __construct(
        QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;

    }

    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        if(!$extAttributes = $addressInformation->getExtensionAttributes()){
            return;
        }

        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setLinkedinProfile($extAttributes->getLinkedinProfile());
        $quote->save();
    }
}
