<?php
namespace Elogic\Linkedin\Plugin;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
class ConfigPlugin
{
    private $eavSetupFactory;

     /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
   protected $scopeConfig;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    protected $attributeSetFactory;

/**
 * @param EavSetupFactory $eavSetupFactory
 * @param CustomerSetupFactory $customerSetupFactory
 * @param ScopeConfigInterface $scopeConfig
 * @param AttributeSetFactory $attributeSetFactory
 * @param ManagerInterface $messageManager
 */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        ScopeConfigInterface $scopeConfig,
        AttributeSetFactory $attributeSetFactory,
        ManagerInterface $messageManager
    ){
        $this->eavSetupFactory = $eavSetupFactory;
        $this->scopeConfig = $scopeConfig;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->messageManager = $messageManager;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    public function afterSave(
        \Magento\Config\Model\Config $subject
    )
    {
        $attributeAfterSave = $this->scopeConfig->getValue(
            'alexfin/settings/linkedin_field_type',
            $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            $scopeCode = null
        );
        $customerSetup = $this->customerSetupFactory->create();
        $eavSetup = $this->eavSetupFactory->create();
        $testBool = false;

        if ($attributeAfterSave == 1)
        {
            $testBool = true;
        }
            $eavSetup->addAttribute(
                \Magento\Customer\Model\Customer::ENTITY,
                'linkedin_profile',
                [
                    'type' => 'varchar',
                    'label' => 'linkedin_profile',
                    'input' => 'text',
                    'validate_rules' => '{"max_text_length":250,"min_text_length":16}',
                    'required' => "$testBool",
                    'sort_order' => 120,
                    'position' => 120,
                    'visible' => true,
                    'user_defined' => true,
                    'unique' => false,
                    'system' => false,
                ]);

        $this->messageManager->addNoticeMessage
        ("Settings is applied by Plugin!, attribute status: . $attributeAfterSave");
        }
}
