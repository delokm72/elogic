var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Elogic_Linkedin/js/view/shipping-mixin': false
            }
        }
    },
    "map": {
        "*": {
            "Magento_Checkout/js/model/shipping-save-processor/default" : "Elogic_Linkedin/js/shipping-save-processor"
        }
    }
};
