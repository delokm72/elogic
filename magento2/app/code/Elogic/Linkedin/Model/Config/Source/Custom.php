<?php

namespace Elogic\Linkedin\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;
/**
 * Class Custom
 * @package Alex\Fin\Model\Config\Source
 */
class Custom implements ArrayInterface
{
    /**
     * Retrieve Custom Option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => 'invisible (NOT WORK YET!)'],
            ['value' => 1, 'label' => 'required'],
            ['value' => 2, 'label' => 'recomended']
        ];
    }
}
