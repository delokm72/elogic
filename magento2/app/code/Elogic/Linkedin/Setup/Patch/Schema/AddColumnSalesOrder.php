<?php

namespace Elogic\Linkedin\Setup\Patch\Schema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class AddColumnSalesOrder implements SchemaPatchInterface
{
    private $moduleDataSetup;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->moduleDataSetup->getConnection()->addColumn(
            $this->moduleDataSetup->getTable('sales_order'),
            'linkedin_profile',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 250,
                'nullable' => true,
                'comment' => 'linkedin_profile',
            ]
        );
        $this->moduleDataSetup->getConnection()->addColumn(
            $this->moduleDataSetup->getTable('sales_order_grid'),
            'linkedin_profile',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 250,
                'nullable' => true,
                'comment' => 'linkedin_profile',
            ]
        );

        $this->moduleDataSetup->endSetup();
    }
}
