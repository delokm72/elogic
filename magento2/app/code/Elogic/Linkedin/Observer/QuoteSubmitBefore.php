<?php
namespace Elogic\Linkedin\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteRepository;

class QuoteSubmitBefore implements ObserverInterface  //copying from quote to sales_order
{
    protected $quoteRepository;

    public function __construct(QuoteRepository $quoteRepository)
    {
        $this->quoteRepository = $quoteRepository;
    }

    public function execute(EventObserver $observer)
    {
        $order = $observer->getOrder();
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $order->setLinkedinProfile( $quote->getLinkedinProfile() );
        return $this;
    }
}
